<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$connected_dtb = mysqli_connect("localhost", "sensorlog", "sensorlog12345", "sensorDtb");
if (!$connected_dtb) {
    $conn_status['dbconnect_status'] = 'failed';
    $conn_status['status'] = mysqli_connect_errno() . PHP_EOL;
    $conn_status['more_info'] = mysqli_connect_error() . PHP_EOL;
    echo (json_encode($conn_status));
    die(); //Exit
}

$export = mysqli_query($connected_dtb, "SELECT * FROM SensorPM") or die("Sql error : " . mysqli_errno($connected_dtb));

//$fields = mysqli_num_fields($export);
//
//for ($i = 0; $i < $fields; $i++) {
//    //  $header .= mysql_field_name($export, $i) . "\t";    
//    $header .= mysqli_field_seek($export, $i) . "\t";
//}
$header .='#' . "\t";
$header .='ID' . "\t";
$header .='NAME' . "\t";
$header .='UTC(sec)' . "\t";
$header .='UTC_TIME' . "\t";
$header .='DO AM' . "\t";
$header .='NHIET DO' . "\t";

while ($row = mysqli_fetch_row($export)) {
    $line = '';
    foreach ($row as $value) {
        if ((!isset($value) ) || ( $value == "" )) {
            $value = "\t";
        } else {
            $value = str_replace('"', '""', $value);
            $value = '"' . $value . '"' . "\t";
        }
        $line .= $value;
    }
    $data .= trim($line) . "\n";
}
$data = str_replace("\r", "", $data);

if ($data == "") {
    $data = "\n(0) Records Found!\n";
}
@mysqli_close($connected_dtb);
$date_readable = gmdate("d_m_Y_h_i_s", time());
$filename = 'EnvMonitor'.$date_readable.'.xls';
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";

