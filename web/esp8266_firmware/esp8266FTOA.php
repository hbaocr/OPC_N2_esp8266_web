<?PHP
//$firmware_version = "@@FIRM_VERSION_1.0!!";
$filename = "/var/www/html/esp8266_firmware/dht_remote_ota.bin";
function getfirmware_version($firm_name) {
    $ret = '';
    $patern = '/@@FIRM_VERSION_(.*?)!!/';
    if (file_exists($firm_name)) {
        $file = file_get_contents($firm_name);
        preg_match($patern, $file, $r);
        //var_dump($r);
        $len_str = strlen($r[0]);
        if (($len_str > 10) && ($len_str < 40)) { //valid value
            $ret = $r[0];
        }
    } else {
        // var_dump('err');
    }
    return $ret;
}
header('Content-type: text/plain; charset=utf8', true);
function check_header($name, $value = false) {
    if (!isset($_SERVER[$name])) {
        return false;
    }
    if ($value && $_SERVER[$name] != $value) {
        return false;
    }
    return true;
}
function sendFile($path) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK', true, 200);
    header('Content-Type: application/octet-stream', true);
    header('Content-Disposition: attachment; filename=' . basename($path));
    header('Content-Length: ' . filesize($path), true);
    header('x-MD5: ' . md5_file($path), true);
    readfile($path);
}

if (!check_header('HTTP_USER_AGENT', 'ESP8266-http-Update')) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 403 Forbidden', true, 403);
    echo "only for ESP8266 updater!\n";
    exit();
}
if (
        !check_header('HTTP_X_ESP8266_STA_MAC') ||
        !check_header('HTTP_X_ESP8266_AP_MAC') ||
        !check_header('HTTP_X_ESP8266_FREE_SPACE') ||
        !check_header('HTTP_X_ESP8266_SKETCH_SIZE') ||
        !check_header('HTTP_X_ESP8266_CHIP_SIZE') ||
        !check_header('HTTP_X_ESP8266_SDK_VERSION') ||
        !check_header('HTTP_X_ESP8266_VERSION')
) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 403 Forbidden', true, 403);
    echo "only for ESP8266 updater! (header)\n";
    exit();
}

if (file_exists($filename)) {
    // echo "The file $filename exists";
    $server_version = getfirmware_version($filename);
    if ($server_version != $_SERVER['HTTP_X_ESP8266_VERSION']) {
        sendFile($filename);
    } else {
        header($_SERVER["SERVER_PROTOCOL"] . ' 304 Not Modified', true, 304);
    }
    exit();
} else {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 no firmware available', true, 500);
}
//header($_SERVER["SERVER_PROTOCOL"] . ' 500 no version for ESP MAC', true, 500);
