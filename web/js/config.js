CONFIG = {
	ENDPOINT:'',
	URL: {
		Ids: 'ids.php',
		data: 'data_resp.php', // test.php or fake/data.json  
		field: 'fake/fields.json'
	},
	DEFAULT_MODE: 'realtime', //'history or realtime',  
	LIMIT_POINT: 1000,
	INTERVAL: 2000, // in seconds,
        GET_IDS_INTERVAL:15000,
	CHART: {
		LABEL: {
			X: 'TIME LINE',
			Y: 'VALUE'
		},
		FORMAT_TIME: "DD/MM/YYYY HH:mm:ss", // http://canvasjs.com/docs/charts/basics-of-creating-html5-chart/formatting-date-time/
		FORMAT_TOOLTIP: {
			X: "DD/MM/YYYY HH:mm:ss",
			Y: "#######"
		}
	},
	TITLE: 'WORKING ENVIRONMENT MONITORING SYSTEM',
}

