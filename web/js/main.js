RTCHART = {
	mode: 'history',
	data:{
		
	},
	charts:{},
	chartDatas:{},
	fields:{},
	intervalRunning: 0, 
        lastRequest:-1,
	init: function(){
		//console.log('init rtchart');
		RTCHART.mode = CONFIG.DEFAULT_MODE;
		
		$('#chart_mode').on('change',function(){
			RTCHART.mode = $(this).val();
			RTCHART._switchMode();
		});
		$('#chart_mode').val(RTCHART.mode);
		RTCHART._switchMode();
		$('#datetimepicker1').datetimepicker();
		$('#datetimepicker2').datetimepicker({
	            useCurrent: false //Important! See issue #1075
	        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        });
        //config default mode
        $('#chart-demo-title').html(CONFIG.TITLE);
        document.title = CONFIG.TITLE;
        $('#limit_point').val(CONFIG.LIMIT_POINT);
        this.loadIds();
        this.loadFields();
        $('#btn-start').on('click',function(){
        	var data = $(this).attr('data');
        	if(data == 0){
        		RTCHART.start();
        	}else{
        		RTCHART.stop();
        	}
        });
        setInterval(function(){
            RTCHART.loadIds();
        },CONFIG.GET_IDS_INTERVAL);
	},
	start: function(){
		var oldId = this.data.id || 0;
		if(!this.validate()){
			return false;
		}
		if(this.mode == 'history'){
			this.reset();
		}
		else{
			if(oldId != this.data.id){
				this.reset();
			}else{
				if(this.intervalRunning){
					clearInterval(this.intervalRunning);
				}
			}
			
		}
		this.disableSelections(true);
		this._toggleButton('#btn-start');
		this.requestChartData(RTCHART._draw);
	},
	stop: function(){
		this.disableSelections(false);
		this._toggleButton('#btn-start');
		if(this.intervalRunning){
			clearInterval(this.intervalRunning);
		}
	},
	disableSelections: function(v){
		$('.input-group-filter select').prop('disabled',v);
		$('.input-group-filter input').prop('disabled',v);
	},
	loadIds: function(){
               
		$.get(RTCHART._getURL(CONFIG.URL.Ids),function(content){
                        content = $.parseJSON(content);
			var data = content.data || [];
                         var p = $('#select_id').parent(); 
                $('#select_id').remove();
                p.prepend(' <select class="form-control" id="select_id"  autocomplete="off"><option value="">Select ID</option></select>');
                console.log(content);
			if(data.length > 0){
				for(i = 0 ; i < data.length; i++){
					var text = data[i].id; 
					if(data[i].name){
						text += " - " + data[i].name;
					}
					$('#select_id').append('<option value="'+data[i].id+'">'+text+'</option>');
				}
			}
		});
	},
	loadFields:function(){
		$.get(RTCHART._getURL(CONFIG.URL.field),function(content){
			var data = content.data || [];
			RTCHART.fields = data; 
		});
	},
	requestChartData: function(cb){
		$.ajax({
			url: RTCHART._getURL(CONFIG.URL.data),
			data:{
				mode: RTCHART.mode,
				data: RTCHART.data
			},
			dataType: "json",
			type: 'POST',
		}).done(function(content){
			//data = $.parseJSON(content);
			//console.log(content.f1);
			//RTCHART._draw(content);
			//console.log(cb);
                        RTCHART.lastRequest = content.time;
			cb.call(this,content.data);
		}).error(function(content){
			alert(content);
		});
	},
	getData: function(){
		var id = $('#select_id').val();
		if(this.mode ==  'history'){
			//var mode = $('#chart_mode').val();
			var start = '';
			if($('#datetimepicker1').data("DateTimePicker").date()){
				start = $('#datetimepicker1').data("DateTimePicker").date().unix();
			}
			var end = '';
			if($('#datetimepicker2').data("DateTimePicker").date()){
				end = $('#datetimepicker2').data("DateTimePicker").date().unix();
			}
			RTCHART.data = {
				id: id, 
				start: start, 
				end: end,
			}
		}else{
			RTCHART.data = {
				id: id, 
				limit: $('#limit_point').val(),
                                time:RTCHART.lastRequest
			}
			//console.log(RTCHART.data);
		}
		//console.log('RTCHART-data-2');
		//console.log(RTCHART.data);
		return RTCHART.data;
	},
	validate: function(){
		this.getData();
		var msg = [];
		if(!this.data.id || this.data.id == ""){
			msg.push("- ID must be selected");
		}
		if(this.mode == 'history'){
			if(this.data.start == ''){
				msg.push("- Start Time must be added");
			}
			if(this.data.end == ''){
				msg.push("- End Time must be added");
			}
		}else{
			if(this.data.limit == ''){
				msg.push("Limit points must be added");
			}
		}
		if(msg.length <=0){
			return true;
		}
		alert(msg.join("\n"));
		return false;
	},
	_draw: function(data){
		for(f in data){
			var fd = data[f];
			var fholder_id = 'chartField-'+f; 
			if($('#' + fholder_id).length <= 0){
				RTCHART.chartDatas[f] = [];
				for(j = 0; j < fd.length; j++){
					RTCHART.chartDatas[f].push({
						x: fd[j].time*1000,
						y: fd[j].value
					});
				}
				var f_name = RTCHART._getFieldName(f);
				$('#charts-holder').append('<div id="' + fholder_id + '" class="fieldchart-holder-view"></div>');
				let chart = new CanvasJS.Chart(fholder_id,{
					title :{
						text: f_name
					},	
					axisX:{
				        title: CONFIG.CHART.LABEL.X,
				        gridThickness: 2,
				        interval:2,        
				        valueFormatString: CONFIG.CHART.FORMAT_TIME, 
				        labelAngle: -30
				    },
				    axisY: {
				        title: CONFIG.CHART.LABEL.Y,
				        includeZero: true,
				    },
					data: [{
						xValueType: "dateTime",
						type: "line",
						xValueFormatString: CONFIG.CHART.FORMAT_TOOLTIP.X,
						yValueFormatString: CONFIG.CHART.FORMAT_TOOLTIP.Y,
						dataPoints: RTCHART.chartDatas[f]
					}],
					zoomEnabled: true, 
				}); 
				RTCHART.charts[f] = chart; 
				RTCHART.charts[f].render();		
			}else{
				var chart = RTCHART.charts[f];
				//var hasUpdate = false;
				//var ofd = RTCHART.chartDatas[f]; 
				for(j = 0; j < fd.length; j++){
					var x = fd[j].time*1000; 
					
					//validate
					var kl = RTCHART.chartDatas[f].length;
					var hasExisted = false;
					for(k = 0; k < kl; k++){
						//console.log(ofd[k].x + '-----' + x);
						//return false;
						if(RTCHART.chartDatas[f][k].x >= x){
							hasExisted = true;
							break;
						}
					}
					if(!hasExisted){
						RTCHART.chartDatas[f].push({
							x: x,
							y: fd[j].value
						});
						if(RTCHART.chartDatas[f].length >= RTCHART.data.limit){
							console.log('shift')
							RTCHART.chartDatas[f].shift();
						}
					}
				}
				//RTCHART.chartDatas[f] = ofd;
				chart.render();
			}
		}
		if(RTCHART.mode == 'realtime'){
			RTCHART.intervalRunning = setTimeout(function(){
                                RTCHART.data.time = RTCHART.lastRequest;
				RTCHART.requestChartData(RTCHART._draw);
			},CONFIG.INTERVAL);
		}
	},
	
	_getFieldName: function(fid){
		for(i = 0; i < this.fields.length; i++){
			var item = this.fields[i];
			if(item.field == fid){
				return item.name;
			}
		}
		return "Field " + fid;
	},
	_switchMode: function(){
		if(this.mode == 'history'){
			$('#limit_point_holder').hide();
			$('#datetimepicker1').show();
			$('#datetimepicker2').show();
		}else{
			$('#datetimepicker1').hide();
			$('#datetimepicker2').hide();
			$('#limit_point_holder').show();
		}
	},
	_toggleButton: function(e){
		var data = $(e).attr('data');
    	data = parseInt(data);
    	data = (data == 0) ? 1 : 0;
    	if(data == 0){
    		$(e).html("Start");
    		$(e).attr('class','btn btn-success');
    	}else{
    		$(e).html("Stop");
    		$(e).attr('class','btn btn-danger');
    	}
    	$(e).attr('data',data);
	},
	_getURL: function(uri){
		if(CONFIG.ENDPOINT == ''){
			return uri;
		}
		return CONFIG.ENDPOINT + uri;
	},
	_formatTime: function(t){
		var dateString = moment.unix(t).format("MM/DD/YYYY H:m");
		return dateString; 
	},
	reset: function(){
		this.chartDatas = {};
		this.charts = {};
		//this.data = {};
		//this.loadIds(); 
		//this.loadFields();
		if(this.intervalRunning){
			clearInterval(this.intervalRunning);
		}
		$('#charts-holder').html("");
	}
}
$(document).ready(function(){
	RTCHART.init();
})