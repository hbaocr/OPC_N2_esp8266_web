<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$EXPECTED_VERSION_NUM = 2;
$HTTP_CODE_NOT_MODIFIED = 304;
$firmwarePath = "./esp8266_firmware/";
$firmware_name = $firmwarePath . 'ESP8226_DTH_OTA_SLEEP.bin';
$version_info = $firmwarePath . 'version_number.txt';
$new_firmware_version = intval(file_get_contents($version_info, FILE_USE_INCLUDE_PATH));

header('Content-type: text/plain; charset=utf8', true);

function check_header($name, $value = false) {
    if (!isset($_SERVER[$name])) {
        return false;
    }
    if ($value && $_SERVER[$name] != $value) {
        return false;
    }
    return true;
}

function sendFile($path) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK', true, 200);
    header('Content-Type: application/octet-stream', true);
    header('Content-Disposition: attachment; filename=' . basename($path));
    header('Content-Length: ' . filesize($path), true);
    header('x-MD5: ' . md5_file($path), true);
    readfile($path);
}

if (!isset($_GET['x-ESP8266-version'])) {
    $current_firmwareVersion = 0;
} else {
    $current_firmwareVersion = intval($_GET['x-ESP8266-version']);
}

if ($new_firmware_version > $current_firmwareVersion) {
    sendFile($firmware_name);
} else {
    http_response_code($HTTP_CODE_NOT_MODIFIED);
}