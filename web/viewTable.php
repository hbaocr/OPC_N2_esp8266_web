<?php

$connected_dtb = mysqli_connect("localhost", "sensorlog", "sensorlog12345", "sensorDtb");
if (!$connected_dtb) {
    $conn_status['dbconnect_status'] = 'failed';
    $conn_status['status'] = mysqli_connect_errno() . PHP_EOL;
    $conn_status['more_info'] = mysqli_connect_error() . PHP_EOL;
    echo (json_encode($conn_status));
    die(); //Exit
}

$result = mysqli_query($connected_dtb, "SELECT * FROM SensorPM");

echo "<table border='1'>";

$i = 0;
while ($row = $result->fetch_assoc()) {
    if ($i == 0) {
        $i++;
        echo "<tr>";
        foreach ($row as $key => $value) {
            echo "<th>" . $key . "</th>";
        }
        echo "</tr>";
    }
    echo "<tr>";
    foreach ($row as $value) {
        echo "<td>" . $value . "</td>";
    }
    echo "</tr>";
}
echo "</table>";

@mysqli_close($connected_dtb);
?>