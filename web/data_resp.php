<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//select * from SensorPM where ID='1213' and UTC >= '120' limit 2
//$REAL_TIME_MODE = 'realtime';
//$rq = isset($_REQUEST['data']) ? $_REQUEST['data'] : array();
//$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : $REAL_TIME_MODE;
//
////Connect to local database
$REAL_TIME_MODE = 'realtime';
$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : $REAL_TIME_MODE;
$data = isset($_REQUEST['data']) ? $_REQUEST['data'] : array();
$time = isset($data['time']) ? $data['time'] : '120';
$id = isset($data['id']) ? $data['id'] : '';
$limit = isset($data['limit']) ? (int) $data['limit'] : 100;
$start_time = isset($data['start']) ? $data['start'] : '1494909132';
$end_time = isset($data['end']) ? $data['end'] : '1494909132';

if (!$id) {
    echo json_encode(array(
        'time' => $time,
        'data' => array()
    ));
    exit;
}
if ($mode == $REAL_TIME_MODE) {
    $last_2h_utc_sec = time() - 7200; //
    $time = ($time < 1494909132) ? $last_2h_utc_sec : $time;
    $sql = "select * from SensorPM where ID='" . $id . "' and UTC_EPOCH > " . $time . " ORDER BY UTC_EPOCH ASC limit  " . $limit;
}
else
{
    $sql = "select * from SensorPM where ID='" . $id . "' and UTC_EPOCH > " . $start_time . " and UTC_EPOCH < ".$end_time." ORDER BY UTC_EPOCH ASC limit  " . $limit; 
}
$connected_dtb = mysqli_connect("localhost", "sensorlog", "sensorlog12345", "sensorDtb");
if (!$connected_dtb) {
    $conn_status['dbconnect_status'] = 'failed';
    $conn_status['status'] = mysqli_connect_errno() . PHP_EOL;
    $conn_status['more_info'] = mysqli_connect_error() . PHP_EOL;
    echo (json_encode($conn_status));
    die(); //Exit
}

//echo $sql;
$retval = mysqli_query($connected_dtb, $sql);
if (!$retval) {
    //mysql_close($connected_dtb);
    die('Could not enter data: ' . mysqli_errno($connected_dtb));
}
$nr = mysqli_num_rows($retval);
$last = $time;
if ($nr > 0) {

    // output data of each row
    $aData = array();

    while ($row = mysqli_fetch_row($retval)) {
        $utc = $row[3];
        for ($i = 1; $i <= 16; $i++) {
            if (!is_null($row[$i + 4])) {
                $ikey = "f$i";
                $tm = $row[$i + 4];
                $tmp = intval($utc);
                $aData[$ikey][] = array(
                    'time' => $tmp,
                    'value' => floatval($tm)
                );
                $last = $tmp;
            }
        }
    }
    http_response_code(200);
    echo json_encode(array(
        'time' => $last,
        'data' => $aData
    ));
    mysqli_free_result($retval);
} else {
    echo json_encode(array(
        'time' => $last,
        'data' => array()
    ));
}

@mysqli_close($connected_dtb);
