<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//select * from SensorPM where ID='1213' and UTC >= '120' limit 2
//$REAL_TIME_MODE = 'realtime';
//$rq = isset($_REQUEST['data']) ? $_REQUEST['data'] : array();
//$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : $REAL_TIME_MODE;
//
////Connect to local database
$connected_dtb = mysqli_connect("localhost", "sensorlog", "sensorlog12345", "sensorDtb");
if (!$connected_dtb) {
    $conn_status['dbconnect_status'] = 'failed';
    $conn_status['status'] = mysqli_connect_errno() . PHP_EOL;
    $conn_status['more_info'] = mysqli_connect_error() . PHP_EOL;
    echo (json_encode($conn_status));
    die(); //Exit
}

$sql = "select ID,NAME from SensorPM group by ID,NAME;";
$retval = mysqli_query($connected_dtb, $sql);

if ( !$retval) {
    //mysql_close($connected_dtb);
    echo mysqli_error($connected_dtb);
    die('Could not enter data: ' . mysqli_errno($connected_dtb));
}
$nr = mysqli_num_rows($retval);
 $last = -1;
if ($nr > 0) {

    // output data of each row
    $aData = array();
   
    while ($row = mysqli_fetch_assoc($retval)) {
        $aData[] = array(
            'id' => $row['ID'],
            'name' => $row['NAME']
        );
    }
    http_response_code(200);
    echo json_encode(array(
       'data' => $aData
    ));
    mysqli_free_result($retval);
    
} else {
    echo json_encode(array(
       'data' => array()
    ));
}

@mysqli_close($connected_dtb);
