/*
Navicat MySQL Data Transfer

Source Server         : Azure
Source Server Version : 50718
Source Host           : 52.169.189.143:3306
Source Database       : sensorDtb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-05-18 11:46:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for SensorPM
-- ----------------------------
DROP TABLE IF EXISTS `SensorPM`;
CREATE TABLE `SensorPM` (
  `NUMBER` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `UTC_EPOCH` int(11) DEFAULT NULL,
  `DATE_DESC` varchar(255) DEFAULT NULL,
  `F1` varchar(255) DEFAULT NULL,
  `F2` varchar(255) DEFAULT NULL,
  `F3` varchar(255) DEFAULT NULL,
  `F4` varchar(255) DEFAULT NULL,
  `F5` varchar(255) DEFAULT NULL,
  `F6` varchar(255) DEFAULT NULL,
  `F7` varchar(255) DEFAULT NULL,
  `F8` varchar(255) DEFAULT NULL,
  `F9` varchar(255) DEFAULT NULL,
  `F10` varchar(255) DEFAULT NULL,
  `F11` varchar(255) DEFAULT NULL,
  `F12` varchar(255) DEFAULT NULL,
  `F13` varchar(255) DEFAULT NULL,
  `F14` varchar(255) DEFAULT NULL,
  `F15` varchar(255) DEFAULT NULL,
  `F16` varchar(255) DEFAULT NULL,
  `LAT_LONG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`NUMBER`)
) ENGINE=InnoDB AUTO_INCREMENT=11923 DEFAULT CHARSET=latin1;
